var mongoose = require('mongoose')
  , Schema = mongoose.Schema
  , crypto = require('crypto')
  ,uuid = require('node-uuid')
  
  , authTypes = [ 'facebook']
  
  
  var count=0;
  var UserSchema = mongoose.Schema({
            salt: {type: String, required: true, default: uuid.v1},
            name: String,
            email: String,
            
            fullName:{type: String,default: 0},
            birth:{type: String,default: 0},
            gender: {type: String,default: 0},
            address:{type: String,default: 0},
            mobile: {type: String,default: 0},
            
            activation:String,
            type: String,
            password: String,
            articleNumber: {type: Number,default: 0},
            userType: {type: String,default: 0},
            about:String,
            profilePicture: {type: String, default: null},
            age: {type: String,default: 0},
            gender: {type: String,default: 0},
            country: {type: String,default: 0},
            facebook: {},
            facebookAccessToken: {},
            pendingArticleCount: {type: Number,default: 0},
            acceptedArticleCount: {type: Number,default: 0},
            rejectedArticleCount: {type: Number,default: 0},
            
            article: [{
                      //articleNumber:{type: Number,default: 0},
                      image:String,
                      title:String,
                      category:String,
                      article:String,
                      status:String,
                      created: {type: Date, default: Date.now}

               }],

    })
    
var hash = function(passwd, salt) {
  
return crypto.createHmac('sha256', salt).update(passwd).digest('hex');
};


UserSchema.methods.setPassword = function(passwordString) {
    
    this.password = hash(passwordString, this.salt);
};
UserSchema.methods.isValidPassword = function(passwordString) {
    return this.password === hash(passwordString, this.salt);
};

 mongoose.model('User',UserSchema);