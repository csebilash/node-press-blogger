var mongoose = require('mongoose')
  , User = mongoose.model('User')
  , fs = require('fs')
  , mandrill = require('node-mandrill')('dlYF7-zjxHaIBVixKuq7XQ')
  , _ = require("underscore")

var x=0;
var lastArticleCount=0;


exports.authCallback = function (req, res, next) {
console.log(req.user.name);


req.user.userType='Author';
console.log(req.user.userType);
req.user.save(function(err){
  if(err)
    console.log('Error');
  else
    console.log('Done');
})
res.redirect('/writeNewArticle')


}
exports.signin = function (req, res) {
  //console.log('Sign In:');
  //console.log(req.body);
  res.render('signIn', {
    title: 'Signin',
    message: req.flash('error')
  })
}
exports.signup = function (req, res) {
  console.log('Test signup');
    res.render('signUp');
}

exports.signout = function (req, res) {
  req.logout()
  res.redirect('/')
}


exports.registration=function(req,res){
  console.log('Registration:');
  var user = new User();
  user.name=req.body.name;
  user.email=req.body.email;
  user.activation='Inactive';
  //user.password=req.body.password;
  user.setPassword(req.body.password);
  user.userType='Author';
  user.save(function(err){
        if(err)
          console.log(err);
        else{
                  mandrill('/messages/send', {


                      "message": {
                          to: [{email: req.body.email}],
                          from_email: 'admin@patarboi.ca',
                          subject: "Registration",
                          "html":  "Your Registration has been successfull. Use this link to activate account : localhost/activation/"+user._id ,
                      },
                     
                    
                  }, function(error, response)
                  {

                      if (error) 
                        console.log( JSON.stringify(error) );

                      else 
                        {
                            console.log("activation mail send");
                            console.log(response);
                            res.redirect('/signin');
                        }
                  });


        }
          
  });
 

} 


exports.activation=function(req,res)
{
    console.log(req.params.user_id);
    userId=req.params.user_id;

    User.findOne({_id:userId}).exec(function(err,user){
        if(err)
            console.log(err);
        else
            {
                console.log(user.email);
                user.activation='Active';
                user.save(function(err){

                    if(err)
                      console.log(err);
                    else
                      res.redirect('/');
                })
            }

    })
}

exports.admin=function(req,res){
var count=0;
User.find().exec(function(err,user){
  if(err)
    console.log(err);
  else
  {
    for(var i=0;i<user.length;i++)
    {
      for(var j=0;j<user[i].article.length;j++)
      {
        if(user[i].article[j].status == 'pending')
          count++;
      }
    }

    res.render('admin',{
        count:count
    });
  }
})


}

exports.profile=function(req,res){

  res.render('author/profile',{
    user:req.user
  });
}

exports.editProfile=function(req,res)
{

  console.log(req.body);
  req.user.fullName=req.body.fName;
  req.user.birth=req.body.birth;
  req.user.gender=req.body.Pgender;
  req.user.address=req.body.address;
  req.user.mobile=req.body.mobile;

  req.user.save(function(err){
      if(err)
        console.log(err);
      else
      {
        res.redirect('/profile');
      }
  })
}

exports.about=function(req,res){
console.log(req.body);
var data=req.body.data;
  User.findOne({userType:'admin'}).exec(function(err,user){
      if(user)
      {
          user.about=data;
          user.save(function(err){
            if(err)
              console.log(err);
            else
            {
              console.log("Saved");
              res.send('Saved');
            }
          })


      }
      
  })
}

exports.writeArticle=function(req,res){
    
    res.render('submit_article');

}
exports.aboutNodepress=function(req,res){
  User.findOne({userType:'admin'}).exec(function(err,user){
      if(user)
      {
          if(req.user == null)
          {
              res.render('about',{
                  login:'0',
                  about:user.about
              });
          }
          else
          {
              res.render('about',{
                  login:'1',
                  about:user.about 
              });
          }


      }
      
  })


}


exports.home=function(req,res){

    if(req.user.userType == 'Author')
    {
        if(req.user.activation == 'Active')
            res.redirect('/');
        else
            res.redirect('/signin#loginError');

    }
    else
    {
      res.redirect("/admin");
    }

}


exports.userArticle=function(req,res){
 var pending=0;
 var publish=0;
 var reject=0;
  console.log(req.user.userType)
  if(req.user.userType == 'admin')
  {
    res.redirect("/admin");
  }
  else
  {

    User.findOne({_id:req.user._id}).exec(function(err,user){
        if(err)
          console.log("Error");
        else if(user)
        {
          console.log("got author");
          for(var i=0;i<user.article.length;i++)
          {
            if(user.article[i].status == "pending")
            {
              pending++;
            }
            else if(user.article[i].status == "Publish")
            {
              publish++;
            }
            else if(user.article[i].status == "Reject")
            {
              reject++;
            }
          }

          res.render('author',{
            user:req.user,
            pending:pending,
            publish:publish,
            reject:reject
          });


        }
    })

  }
  

}

exports.pending_author=function(req,res)
{
    res.render('pendingArticle',{
      user:req.user
    });
}

exports.publish_author=function(req,res)
{

    res.render('publishArticle',{
      user:req.user
    });
}

exports.reject_author=function(req,res)
{

    console.log("Authors Reject article");
    res.render('rejectArticle',{
      user:req.user
    });
}


exports.manage=function(req,res)
{
  console.log("manage");
  res.render('admin/manage');
}

exports.session = function (req, res) {
  console.log('Session:');
  console.log(req.body);
 /* var url=req.session.redirectUrl || '/';
  req.session.redirectUrl=null;
  console.log(req.session.redirectUrl);
  console.log(url);
  res.redirect(url);  */
  
}



exports.facebookSignup = function(req,res){
  
  res.redirect('/auth/facebook');


}


exports.postArticle=function(req,res){
  console.log("got it")
  console.log(req.body);

    fs.readFile(req.files.image.path, function (err, data) {
    
      fs.writeFile('public/img/'+req.files.image.name,data, function (err) {
        if (err) 
          console.log('Error');
          //throw err; 
        else
        {
          console.log("Image Saved Successfully");
          req.user.article.push({title:req.body.title,category:req.body.category,article:req.body.articleDetails,image:req.files.image.name,status:"pending"});
          req.user.pendingArticleCount=req.user.pendingArticleCount+1;
          req.user.save(function(err){
            if(err)
              console.log("error");
            else
            {
              console.log("save article");
              res.redirect('/author');

            }
          });
        }

      });
  });
}

exports.previousArticle=function(req,res)
{
  res.render("previous",{
    user:req.user
  });
}

exports.allArticle=function(req,res)
{
  User.find().exec(function(err,user){
    if(err)
      console.log(err);
    else
    {
      res.render("all_article",{
        alluser:user
      });
    }
  });
}


exports.publish_article=function(req,res){
  console.log(req.body);
  User.findOne({_id:req.body.author_id}).exec(function(err,user){
      if(err)
      {
        console.log(err);

      }
      else if(user)
      {
        console.log("got user");
        for(var i=0;i<user.article.length;i++)
        {
            if(user.article[i]._id == req.body.article_id)
            {
                user.article[i].status = "Publish";
                user.save(function(err){
                  if(!err)
                    {
                      console.log("Saved");
                      res.send("Changed")

                    }
                })
            }
        }

      }
  });
}

exports.reject_article=function(req,res)
{
  console.log(req.body);
  User.findOne({_id:req.body.author_id}).exec(function(err,user){
      if(err)
      {
        console.log(err);

      }
      else if(user)
      {
        console.log("got user");
        for(var i=0;i<user.article.length;i++)
        {
            if(user.article[i]._id == req.body.article_id)
            {
                user.article[i].status = "Reject";
                user.save(function(err){
                  if(!err)
                    {
                      console.log("Saved");
                      res.send("rejected")

                    }
                })
            }
        }

      }
  });

}

exports.adminShowArticle=function(req,res)
{


  console.log(req.body);
  User.findOne({_id:req.body.author_id}).exec(function(err,user){
    if(err)
      console.log(err);
    else if(user)
    {
      console.log("Yes got user");
      console.log(user.article.length)
        for(var i=0;i<user.article.length;i++)
        {
            if(user.article[i]._id == req.body.article_id)
            {
              console.log("yes")
                res.render("article",{
                  article:user.article[i]
                })
            }
        }
    }
  })
}


























