
var async = require('async')



module.exports = function (app, passport, auth) {

var index = require('../app/controllers/index');
var users = require('../app/controllers/users');


//get methods :   
app.get('/signin', users.signin);
app.get('/signup', users.signup);
app.get('/activation/:user_id',users.activation);
app.get('/author',auth.requiresLogin,users.userArticle);
app.get('/admin',auth.requiresLogin, users.admin);
app.get('/author/write-article',auth.requiresLogin,users.writeArticle);
app.get('/signout',users.signout);
app.get('/auth/facebook', passport.authenticate('facebook', { scope: [ 'email', 'user_about_me', 'read_stream', 'publish_actions'], failureRedirect: '/' }));
app.get('/auth/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/' }), users.authCallback);
app.get('/author/previous-article',auth.requiresLogin,users.previousArticle);
app.get('/admin/all-article',auth.requiresLogin,users.allArticle);
app.get('/pending_author',auth.requiresLogin,users.pending_author);
app.get('/publish_author',auth.requiresLogin,users.publish_author);
app.get('/reject_author',auth.requiresLogin,users.reject_author);
app.get('/manage',auth.requiresLogin,users.manage);
app.get('/about',auth.requiresLogin,users.aboutNodepress);
app.get('/profile',auth.requiresLogin,users.profile);
app.get('/', index.render);



//post methods:
app.post('/users/registration', users.registration);
app.post('/users/session', passport.authenticate('local', {failureRedirect: '/signin#loginError', failureFlash: 'Invalid email or password.'}), users.home);
app.post('/submit_article',auth.requiresLogin,users.postArticle);
app.post('/author/updateProfile',auth.requiresLogin,users.editProfile);

app.post('/admin/publish-article',auth.requiresLogin,users.publish_article);
app.post('/admin/reject-article',auth.requiresLogin,users.reject_article);
app.post('/admin/all-article/article',auth.requiresLogin,users.adminShowArticle);
app.post('/admin/about',users.about);


}
